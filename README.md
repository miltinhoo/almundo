## ALMUNDO CALL CENTER

Existe un call center donde hay 3 tipos de empleados: operador, supervisor y director. 
El proceso de la atención de una llamada telefónica en primera instancia debe ser atendida por un operador,
si no hay ninguno libre debe ser atendida por un supervisor,
y de no haber tampoco supervisores libres debe ser atendida porun director.

## TENER EN CUENTA
-- El proyecto fue construido en Eclipse Jee Oxygen, con Java 8 y utiliza Maven para la gestión de dependencias.

-- Para los test se utiliza Junit 3.8.1

-- Al correr la prueba unitaria sinLlamadasAtendidas() se muestra un error en consonla, este se presenta porque
no se carga el json que contiene los datos de los empleados, este error esta controlado y es creado con esa
intención; **La prueba unitaria corre correctamente.

-- Para cambiar el número de reintentos que se utilizan en la aplicación al lanzarla desde el Main, ir al archivo de propiedades
y editar la variable reintentos.

## PARA EMPEZAR
**Para iniciar la aplicación

Existe un metodo Main ubicado en la clase App.java
Existe una clase de pruebas unitarias llamada DispatcherTest.java

**Datos de entrada

Los datos de entrada son la información de los empleados y la información de las llamadas que entran;
los empleados pueden ser operadores, supervisores y directores; la aplicación lee estos datos desde
archivos .json, esto para simular servicios que nos traen los datos de los empleados y de las llamadas.

**Editar datos de entrada

Los datos de entrada se pueden editar en los .json que se encuentran en la carpeta resources, en caso de
querer incluir otros json que contengan estos datos se recomienda guardarlos en la misma carpeta y cambar el nombre
del archivo en el archivo config.properties, bien sea json-empleados=Nuevo nombre o json-llamadas=Nuevo nombre; en
el caso de las pruebas unitarias tener en cuenta tambien json-empleados-10=Nuevo nombre y json-llamadas-15=Nuevo nombre.

**Clases especiales

-- Se crea la clase Dispatcher para el manejo de las llamadas, esta implementa Callable, la funcion call returna un objeto
llamado CallResponse el cual contiene una lista de llamadas perdidas y una lista de llamadas atendidas.

-- Se crea la clase LinkedListSynchronized que extiende de LinkedList para controlar la concurrencia a los metodos utilizados,
especialmente a los metodos que modifican la lista.

-- Se crea la case ConfigProperties para acceder al archivo de propiedades, esta se crea como Singleton, ya que no es necesario
estar creando instancias de la misma.

-- Se crean las clases DataCall y DataEmployee para instanciar los datos de las llamadas y empleados respectivamente, estas
clases extienden a su ves de la clase abstracta CreateData; esta ultima se crea ya que tanto DataCall y DataEmployee tienen
comportamientos en común.

**Extras/Plus

--Dar alguna solución sobre qué pasa con una llamada cuando no hay ningún empleado libre.
Para este caso se crea un atributo de Dispatcher llamado intentos, este es un entero que puede iniciar en cero
cuando no hay empleados libres el hilo queda en espera de que alguna llamada sea atendida en el metodo atenderLlamada()
y este con un notify informe a ese hilo que puede continuar el proceso para buscar un empleado que pueda atender la
llamada, si se supera el número de intentos se retornan las llamadas no atendidas en el objeto CallResponse.
**VER los test esperarPorOperarioLibre(), sinLlamadasAtendidas() y llamadasPerdidas()

--Dar alguna solución sobre qué pasa con una llamada cuando entran más de 10 llamadas concurrentes.
Para este caso la solución es similar a la anterior, primero se guardan todas las llamadas en una colección de tipo
LinkedListSynchronized, el sistema antiende 10 llamadas de la cola simultaneamente tomando una a una y buscando empleados
disponibles para tomarlas, en caso de que alguna no pueda ser tomada se aplica la solución del punto
anterior, cuando se libera uno de los 10 hilos el sistema incluye otro y asi sucesivamente hasta terminar con los datos
del arreglo. **Ver el test atenderMultiplesLlamadas()

## CREADO POR: Milton Velilla Cataño, profile [HackerRank](https://www.hackerrank.com/miltonvelilla)