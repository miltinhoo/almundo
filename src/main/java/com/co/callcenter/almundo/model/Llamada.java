package com.co.callcenter.almundo.model;

public class Llamada {
	private long id;
	private int intentos;
	
	public Llamada() {
		super();
	}
	
	public Llamada(long id, int intentos) {
		super();
		this.id = id;
		this.intentos = intentos;
	}
	
	public synchronized long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public synchronized int getIntentos() {
		return intentos;
	}
	public synchronized void setIntentos(int intentos) {
		this.intentos = intentos;
	}
}
