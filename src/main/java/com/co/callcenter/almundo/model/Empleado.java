package com.co.callcenter.almundo.model;

public class Empleado {
	private String documentoIdentidad;
	private String nombre;
	private String tipoEmpleado;	
	
	public Empleado() {
		super();
	}
	
	public Empleado(String documentoIdentidad, String nombre, String tipoEmpleado) {
		super();
		this.documentoIdentidad = documentoIdentidad;
		this.nombre = nombre;
		this.tipoEmpleado = tipoEmpleado;
	}
	
	public String getDocumentoIdentidad() {
		return documentoIdentidad;
	}
	public void setDocumentoIdentidad(String documentoIdentidad) {
		this.documentoIdentidad = documentoIdentidad;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getTipoEmpleado() {
		return tipoEmpleado;
	}
	public void setTipoEmpleado(String tipoEmpleado) {
		this.tipoEmpleado = tipoEmpleado;
	}	
}
