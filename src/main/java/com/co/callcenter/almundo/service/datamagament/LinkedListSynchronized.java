package com.co.callcenter.almundo.service.datamagament;

import java.util.Collection;
import java.util.LinkedList;

import com.co.callcenter.almundo.model.Empleado;
import com.co.callcenter.almundo.model.Llamada;

public class LinkedListSynchronized extends LinkedList{
	public LinkedListSynchronized() {
		super();
	}

	public LinkedListSynchronized(Collection c) {
		super(c);
	}
	
	public synchronized boolean isEmpty() {
		return super.isEmpty();
	}	
	
	public synchronized boolean add(Object e) {
		return super.add(e);
	}
	
	public synchronized void addLast(Object e) {
		super.addLast(e);
	}
	
	public synchronized void addFirst(Object e) {
		super.addFirst(e);
	}
	
	public synchronized Empleado removeFirstEmpleado() {
		return (Empleado)super.removeFirst();
	}
	
	public synchronized Llamada removeFirstLlamada() {
		return (Llamada)super.removeFirst();
	}
}
