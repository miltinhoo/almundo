package com.co.callcenter.almundo.service.datamagament;

import com.co.callcenter.almundo.model.Llamada;

public class CallResponse {
	private LinkedListSynchronized succesfulCalls;
	private LinkedListSynchronized lostCalls;
	
	public CallResponse() {
		this.succesfulCalls = new LinkedListSynchronized();
		this.lostCalls = new LinkedListSynchronized();
	}
	
	public LinkedListSynchronized getSuccesfulCalls() {
		return succesfulCalls;
	}
	
	public void setSuccesfulCalls(LinkedListSynchronized succesfulCalls) {
		this.succesfulCalls = succesfulCalls;
	}
	
	public void setSuccesfulIndividualCalls(Llamada succesfulCall) {
		this.succesfulCalls.add(succesfulCall);
	}
	
	public LinkedListSynchronized getLostCalls() {
		return lostCalls;
	}
	
	public void setLostCalls(LinkedListSynchronized lostCalls) {
		this.lostCalls = lostCalls;
	}
	
	public void setLostIndividualCalls(Llamada lostCall) {
		this.lostCalls.add(lostCall);
	}
}
