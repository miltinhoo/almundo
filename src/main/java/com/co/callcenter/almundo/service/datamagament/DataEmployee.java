package com.co.callcenter.almundo.service.datamagament;

import org.json.simple.JSONObject;

import com.co.callcenter.almundo.model.Empleado;
import com.co.callcenter.almundo.utilities.ConfigProperties;
import com.google.gson.Gson;

public class DataEmployee extends CreateData {
	private LinkedListSynchronized operadores;
	private LinkedListSynchronized supervisores;
	private LinkedListSynchronized directores;
	private String fileName;
	
	public DataEmployee(String fileName) {
		super();
		this.operadores = new LinkedListSynchronized();
		this.supervisores = new LinkedListSynchronized();
		this.directores = new LinkedListSynchronized();
		this.fileName = fileName;
	}

	public LinkedListSynchronized getOperadores() {
		return operadores;
	}

	public void setOperadores(LinkedListSynchronized operadores) {
		this.operadores = operadores;
	}

	public LinkedListSynchronized getSupervisores() {
		return supervisores;
	}

	public void setSupervisores(LinkedListSynchronized supervisores) {
		this.supervisores = supervisores;
	}

	public LinkedListSynchronized getDirectores() {
		return directores;
	}

	public void setDirectores(LinkedListSynchronized directores) {
		this.directores = directores;
	}
	
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	public String getFileName() {
		return this.fileName != null ? this.fileName: configProperties.getPropertie("json-empleados-generico");
	}
	
	public void setDataToObject(Gson gson, JSONObject jsonObject) {
		Empleado empleado = gson.fromJson(jsonObject.toJSONString(), Empleado.class);	
		if(configProperties.getPropertie("operador").equals(empleado.getTipoEmpleado())) {
			operadores.add(empleado);
		}  else if(configProperties.getPropertie("supervisor").equals(empleado.getTipoEmpleado())) {
			supervisores.add(empleado);
		} else if(configProperties.getPropertie("director").equals(empleado.getTipoEmpleado())) {
			this.directores.add(empleado);
		}
	}
}
