package com.co.callcenter.almundo.service.datamagament;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.co.callcenter.almundo.utilities.ConfigProperties;
import com.google.gson.Gson;


public abstract class CreateData {
	ConfigProperties configProperties = ConfigProperties.getInstance();
	private String fileName;
	
	public JSONArray generateData() throws ParseException {
		JSONArray data = null;
		try {
			JSONParser parser = new JSONParser();
			ClassLoader classLoader = getClass().getClassLoader();
			File file = new File(classLoader.getResource(this.getFileName()).getFile());
			data = (JSONArray) parser.parse(new FileReader( file.getAbsolutePath()));
			
			for (Object object : data) {
				try {
					JSONObject jsonObject = (JSONObject) object;
					Gson gson = new Gson();
					this.setDataToObject(gson, jsonObject);					
				} catch (Exception e) {
					System.err.println(configProperties.getPropertie("error-json-parse"));
				}				
			}
			
		} catch (FileNotFoundException e) {
			System.err.println(configProperties.getPropertie("error-archivo-no-encontrado") + " " +getFileName());
			e.printStackTrace();			
		} catch (IOException e) {
			System.err.println(configProperties.getPropertie("error-leer-archivo") + " " +getFileName());
			e.printStackTrace();
		} catch (Exception e) {
			System.err.println(configProperties.getPropertie("error-json-parse") + e.getMessage());
		}
		return data;
	}
	
	public abstract void setDataToObject(Gson gson, JSONObject jsonObject);	
	public abstract String getFileName();
}
