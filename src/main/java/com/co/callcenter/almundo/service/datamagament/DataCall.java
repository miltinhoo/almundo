package com.co.callcenter.almundo.service.datamagament;

import org.json.simple.JSONObject;

import com.co.callcenter.almundo.model.Llamada;
import com.co.callcenter.almundo.utilities.ConfigProperties;
import com.google.gson.Gson;

public class DataCall extends CreateData {
	private LinkedListSynchronized llamadas = new LinkedListSynchronized();
	private String fileName;	
	
	public DataCall(String fileName) {
		super();
		this.fileName = fileName;
	}

	public LinkedListSynchronized getLlamadas() {
		return llamadas;
	}

	public void setLlamadas(LinkedListSynchronized llamadas) {
		this.llamadas = llamadas;
	}
	
	public String getFileName() {
		return this.fileName != null ? this.fileName: configProperties.getPropertie("json-llamadas-generico");
	}
	
	public void setDataToObject(Gson gson, JSONObject jsonObject) {
		Llamada llamada = gson.fromJson(jsonObject.toJSONString(), Llamada.class);
		this.llamadas.add(llamada);				
	}
	
}
