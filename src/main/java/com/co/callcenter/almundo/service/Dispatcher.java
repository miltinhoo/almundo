package com.co.callcenter.almundo.service;

import java.util.Random;
import java.util.concurrent.Callable;
import org.json.simple.parser.ParseException;

import com.co.callcenter.almundo.model.Empleado;
import com.co.callcenter.almundo.model.Llamada;
import com.co.callcenter.almundo.service.datamagament.CallResponse;
import com.co.callcenter.almundo.service.datamagament.DataCall;
import com.co.callcenter.almundo.service.datamagament.DataEmployee;
import com.co.callcenter.almundo.service.datamagament.LinkedListSynchronized;
import com.co.callcenter.almundo.utilities.ConfigProperties;
import com.co.callcenter.almundo.utilities.Interruptor;

public class Dispatcher implements Callable<CallResponse> {
	private LinkedListSynchronized operadores;
	private LinkedListSynchronized supervisores;
	private LinkedListSynchronized directores;
	private LinkedListSynchronized llamadas;
	private CallResponse callResponse;
	private boolean bloqueado;
	private int reintentos;
	private Interruptor interruptor;
	private long callSize;
	private ConfigProperties configProperties;
	
	public Dispatcher() {
		super();
		this.operadores = new LinkedListSynchronized();
		this.supervisores = new LinkedListSynchronized();
		this.directores = new LinkedListSynchronized();
		this.llamadas = new LinkedListSynchronized();		
		this.callResponse = new CallResponse();
		this.bloqueado = false;
		this.reintentos = 0;
		this.callSize = 0;
		this.configProperties = ConfigProperties.getInstance();
	}
	
	public LinkedListSynchronized getOperadores() {
		return operadores;
	}
	
	public void setOperadores(LinkedListSynchronized operadores) {
		this.operadores = operadores;
	}

	public LinkedListSynchronized getSupervisores() {
		return supervisores;
	}

	public void setSupervisores(LinkedListSynchronized supervisores) {
		this.supervisores = supervisores;
	}

	public LinkedListSynchronized getDirectores() {
		return directores;
	}

	public void setDirectores(LinkedListSynchronized directores) {
		this.directores = directores;
	}

	public synchronized void setBloqueado(boolean bloqueado) {
		this.bloqueado = bloqueado;
	}
	
	public boolean isBloqueado() {
		return this.bloqueado;
	}	
	
	public LinkedListSynchronized getLlamadas() {
		return llamadas;
	}

	public void setLlamadas(LinkedListSynchronized llamadas) {
		this.llamadas = llamadas;
	}

	public int getReintentos() {
		return reintentos;
	}	
	
	public void setReintentos(int intentos) {
		this.reintentos = intentos;
	}	

	public Interruptor getInterruptor() {
		return interruptor;
	}	
	
	public void setInterruptor(Interruptor interruptor) {
		this.interruptor = interruptor;
	}	

	public long getCallSize() {
		return callSize;
	}	
	
	public void setCallSize(long callSize) {
		this.callSize = callSize;
	}
		
	public synchronized Empleado getEmpleadoDisponible() {
		Empleado empleado = null;
		if(!this.operadores.isEmpty()) {
			empleado = this.operadores.removeFirstEmpleado();
		} else if(!this.supervisores.isEmpty()) {
			empleado = this.supervisores.removeFirstEmpleado();
		} else if(!this.directores.isEmpty()) {
			empleado = this.directores.removeFirstEmpleado();
		}		
		return empleado;
	}	
	
	public boolean atenderLlamada(Empleado empleado) {
		boolean llamadaAtendida = false;
		try {
			if(empleado != null) {
				Random r = new Random();			
				try {
					int callTime = (r.nextInt(10 -5) + 5) * 1000;
					Thread.sleep(callTime);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
				if(configProperties.getPropertie("operador").equals(empleado.getTipoEmpleado())) {
					this.operadores.addLast(empleado);
				} else if(configProperties.getPropertie("supervisor").equals(empleado.getTipoEmpleado())) {
					this.supervisores.addLast(empleado);
				} else if(configProperties.getPropertie("director").equals(empleado.getTipoEmpleado())) {
					this.directores.addLast(empleado);
				}
				
				llamadaAtendida = true;
				if(this.isBloqueado()) {
					this.setBloqueado(false);
					synchronized (this) {
						this.notify();
					}				
				}			
			}
		} catch (Exception e) {
			System.err.println(configProperties.getPropertie("error-atendiendo-llamada"));
			e.printStackTrace();
		}		
		return llamadaAtendida;
	}

	public boolean dispatchCaller() {
		Empleado empleado = getEmpleadoDisponible();		
		return atenderLlamada(empleado);
	}	
	
	public CallResponse call() {
		
		while(!llamadas.isEmpty()) {
			Llamada llamada = llamadas.removeFirstLlamada();
			if(!this.dispatchCaller()) {
				llamada.setIntentos(llamada.getIntentos() + 1);
				if(llamada.getIntentos() < this.reintentos) {
					llamadas.addFirst(llamada);
					
					try {
						this.setBloqueado(true);
						synchronized (this) {
							this.wait();
						}
					} catch (InterruptedException e) {
						e.printStackTrace();
						System.err.println(configProperties.getPropertie("error-wait-call"));
					}
				} else {
					callResponse.setLostIndividualCalls(llamada);
				}
			} else {
				callResponse.setSuccesfulIndividualCalls(llamada);
			}			
		}		
		if(interruptor != null) {
			synchronized (interruptor) {
				int suma = callResponse.getSuccesfulCalls().size() + callResponse.getLostCalls().size();
				if(suma == callSize) {
					interruptor.notify();
				}
			}	
		}	
		return callResponse;
	}	
	
	public void setDataEmpleadosFromJson(String fileName) {			
		try {
			DataEmployee dataEmployee = new DataEmployee(fileName);
			dataEmployee.generateData();
			this.setOperadores(dataEmployee.getOperadores());
			this.setDirectores(dataEmployee.getDirectores());
			this.setSupervisores(dataEmployee.getSupervisores());
			System.out.println(configProperties.getPropertie("exito-carga-empleados"));
		} catch (ParseException e) {
			System.err.println(configProperties.getPropertie("error-carga-empleados"));
			e.printStackTrace();
		}
	}	
	
	public void setDataCallFromJson(String fileName) {
		try {
			DataCall dataCall = new DataCall(fileName);
			dataCall.generateData();
			this.setLlamadas(dataCall.getLlamadas());
			System.out.println(configProperties.getPropertie("exito-carga-llamadas"));
		} catch (ParseException e) {
			System.err.println(configProperties.getPropertie("error-carga-llamadas"));
			e.printStackTrace();
		}
	}
}
