package com.co.callcenter.almundo.controller;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import com.co.callcenter.almundo.model.Llamada;
import com.co.callcenter.almundo.service.Dispatcher;
import com.co.callcenter.almundo.service.datamagament.CallResponse;
import com.co.callcenter.almundo.utilities.ConfigProperties;
import com.co.callcenter.almundo.utilities.Interruptor;

public class LlamadaController {
	private ConfigProperties configProperties;
	
	public LlamadaController() {
		super();
		this.configProperties = ConfigProperties.getInstance();
	}

	public LinkedList<Llamada> gestionarLlamadas() {
		List<Future<CallResponse>> listResponse = new ArrayList<Future<CallResponse>>();
		Interruptor interruptor = new Interruptor();
		int reintentos;
		try {
			String intentosConfig = configProperties.getPropertie("reintentos");
			reintentos = Integer.parseInt(intentosConfig);
		} catch (Exception e) {
			System.err.println(configProperties.getPropertie("error-cargar-intentos") + e.getMessage());
			e.printStackTrace();
			reintentos = 0;
		}
		
		ejecutarLlamadas(interruptor, listResponse, configProperties.getPropertie("json-empleados"), configProperties.getPropertie("json-llamadas"), reintentos);
		LinkedList<Llamada> llamadasPerdidas = new LinkedList<>();
		
		try {
	    	synchronized (interruptor) {
	    		interruptor.wait();
	    		Future<CallResponse> listaCallResponse = listResponse.get(0);
		    	CallResponse callResponse = listaCallResponse.get(60000, TimeUnit.MILLISECONDS);
		    	llamadasPerdidas = callResponse.getLostCalls();		    	
	    	}
		} catch (InterruptedException | ExecutionException e) {
			System.err.println(configProperties.getPropertie("error-resultado") + e.getMessage());
			e.printStackTrace();
		} catch (TimeoutException e) {
			System.err.println("Time out " + e.getMessage());
			e.printStackTrace();
		}
		
		return llamadasPerdidas;
	}
	
	public void ejecutarLlamadas(Interruptor interruptor, List<Future<CallResponse>> listResponse,
		String fileNameEmployee, String fileNameCall, int reintentos) {
		ExecutorService executor = Executors.newFixedThreadPool(10);
		Dispatcher dispatcher = new Dispatcher();
	    dispatcher.setDataEmpleadosFromJson(fileNameEmployee);
	    dispatcher.setDataCallFromJson(fileNameCall);
	    dispatcher.setReintentos(reintentos);
	    dispatcher.setCallSize(dispatcher.getLlamadas().size());
	    
	    Future<CallResponse> resultado = null;	    
	    dispatcher.setInterruptor(interruptor);
	    for (int i = 0; i < dispatcher.getLlamadas().size(); i++) {
	    	 resultado = executor.submit(dispatcher);
	    	 listResponse.add(resultado);
	    }
	    executor.shutdown();
	}
}
