package com.co.callcenter.almundo.utilities;

import java.io.IOException;
import java.util.Properties;

public class ConfigProperties {
	
	private static ConfigProperties instance = null;
	private final Properties properties = new Properties();
	
	private ConfigProperties() {
		try {
			properties.load(this.getClass().getResourceAsStream("/config.properties"));
		} catch (IOException e) {
			System.err.println("Error al cargar el archivo de propiedades");
			e.printStackTrace();
		}
	}
	
	public static ConfigProperties getInstance() {
		if(instance == null) {
			instance = new ConfigProperties();
		}
		return instance;
	}
	
	public String getPropertie(String key) {
		try {
			return this.properties.getProperty(key);
		} catch (Exception e) {
			System.err.println("Error accediendo a la propiedad con key " + key);
			e.printStackTrace();
			return "";
		}
	}
	
}
