package com.co.callcenter.almundo;

import java.util.LinkedList;

import com.co.callcenter.almundo.controller.LlamadaController;
import com.co.callcenter.almundo.model.Llamada;
import com.co.callcenter.almundo.utilities.ConfigProperties;

public class App 
{
	private static ConfigProperties configProperties = ConfigProperties.getInstance();
    public static void main( String[] args )
    {
    	LlamadaController llamadaController = new LlamadaController();
		LinkedList<Llamada> llamadasPerdidas = llamadaController.gestionarLlamadas();
		llamadasPerdidas.forEach(x -> System.out.println(configProperties.getPropertie("llamada-no-atendida") + " " + x.getId()));
    }
}
