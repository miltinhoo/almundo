package com.co.callcenter.almundo.service;

import org.junit.Test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import com.co.callcenter.almundo.service.Dispatcher;
import com.co.callcenter.almundo.service.datamagament.CallResponse;
import com.co.callcenter.almundo.utilities.ConfigProperties;
import com.co.callcenter.almundo.utilities.Interruptor;

public class DispatcherTest {
	private ConfigProperties configProperties = ConfigProperties.getInstance();
	
	@Test
	public void llamadasPerdidas() {
		List<Future<CallResponse>> listResponse = new ArrayList<Future<CallResponse>>();
		Interruptor interruptor = new Interruptor();
		
		this.ejecutarLlamadas(interruptor, listResponse, configProperties.getPropertie("json-empleados"), configProperties.getPropertie("json-llamadas"), 0);
	    
	    try {
	    	synchronized (interruptor) {
	    		interruptor.wait();
	    		Future<CallResponse> listaCallResponse = listResponse.get(0);
		    	CallResponse callResponse = listaCallResponse.get();	    	
		    	assertTrue(callResponse.getLostCalls().size() == 7);
		    	assertTrue(callResponse.getSuccesfulCalls().size() == 3);
	    	}
		} catch (InterruptedException | ExecutionException e1) {
			fail(configProperties.getPropertie("error-resultado") + " " + e1.getMessage());
			e1.printStackTrace();
		}
	    System.out.println("Finish test llamadasPerdidas----------------------");
	}
	
	@Test
	public void sinLlamadasPerdidas() {	    
	    List<Future<CallResponse>> listResponse = new ArrayList<Future<CallResponse>>();
		Interruptor interruptor = new Interruptor();
		
		this.ejecutarLlamadas(interruptor, listResponse, configProperties.getPropertie("json-empleados-10"), configProperties.getPropertie("json-llamadas"), 0);
	    
	    try {
	    	synchronized (interruptor) {
	    		interruptor.wait();
	    		Future<CallResponse> listaCallResponse = listResponse.get(0);
		    	CallResponse callResponse = listaCallResponse.get();	    	
		    	assertTrue(callResponse.getLostCalls().size() == 0);
		    	assertTrue(callResponse.getSuccesfulCalls().size() == 10);
	    	}
		} catch (InterruptedException | ExecutionException e1) {
			fail(configProperties.getPropertie("error-resultado") + " " + e1.getMessage());
			e1.printStackTrace();
		}
	    System.out.println("Finish test sinLlamadasPerdidas----------------------");
	}
	
	@Test
	public void sinLlamadasAtendidas() {
		List<Future<CallResponse>> listResponse = new ArrayList<Future<CallResponse>>();
		Interruptor interruptor = new Interruptor();
		
		this.ejecutarLlamadas(interruptor, listResponse, null, configProperties.getPropertie("json-llamadas"), 0);
		
		try {
	    	synchronized (interruptor) {
	    		interruptor.wait();
	    		Future<CallResponse> listaCallResponse = listResponse.get(0);
		    	CallResponse callResponse = listaCallResponse.get();	    	
		    	assertTrue(callResponse.getLostCalls().size() == 10);
		    	assertTrue(callResponse.getSuccesfulCalls().size() == 0);
	    	}
		} catch (InterruptedException | ExecutionException e1) {
			fail(configProperties.getPropertie("error-resultado") + " " + e1.getMessage());
			e1.printStackTrace();
		}
	    System.out.println("Finish test sinLlamadasAtendidas----------------------");
	}
	
	@Test
	public void esperarPorOperarioLibre() {
		List<Future<CallResponse>> listResponse = new ArrayList<Future<CallResponse>>();
		Interruptor interruptor = new Interruptor();
		
		this.ejecutarLlamadas(interruptor, listResponse, configProperties.getPropertie("json-empleados"), configProperties.getPropertie("json-llamadas"), 2);
	    
	    try {
	    	synchronized (interruptor) {
	    		interruptor.wait();
	    		Future<CallResponse> listaCallResponse = listResponse.get(0);
		    	CallResponse callResponse = listaCallResponse.get(60000, TimeUnit.MILLISECONDS);
		    	assertTrue(callResponse.getLostCalls().size() < 7);
		    	assertTrue(callResponse.getSuccesfulCalls().size() > 3);
	    	}
		} catch (InterruptedException | ExecutionException e1) {
			fail(configProperties.getPropertie("error-resultado") + " " + e1.getMessage());
			e1.printStackTrace();
		} catch (TimeoutException e) {
			fail(configProperties.getPropertie("error-time-out"));
			e.printStackTrace();
		}
	    System.out.println("Finish test esperarPorOperarioLibre----------------------");
	}
	
	@Test
	public void atenderMultiplesLlamadas() {
		List<Future<CallResponse>> listResponse = new ArrayList<Future<CallResponse>>();
		Interruptor interruptor = new Interruptor();
		
		this.ejecutarLlamadas(interruptor, listResponse, configProperties.getPropertie("json-empleados-10"), configProperties.getPropertie("json-llamadas-15"), 2);
	    
	    try {
	    	synchronized (interruptor) {
	    		interruptor.wait();
	    		Future<CallResponse> listaCallResponse = listResponse.get(0);
		    	CallResponse callResponse = listaCallResponse.get(60000, TimeUnit.MILLISECONDS);
		    	assertTrue(callResponse.getLostCalls().size() == 0);
		    	assertTrue(callResponse.getSuccesfulCalls().size() == 15);
	    	}
		} catch (InterruptedException | ExecutionException e1) {
			fail(configProperties.getPropertie("error-resultado") + " " + e1.getMessage());
			e1.printStackTrace();
		} catch (TimeoutException e) {
			fail(configProperties.getPropertie("error-time-out"));
			e.printStackTrace();
		}
	    System.out.println("Finish test atenderMultiplesLlamadas----------------------");
	}
	
	private void ejecutarLlamadas(Interruptor interruptor, List<Future<CallResponse>> listResponse,
		String fileNameEmployee, String fileNameCall, int reintentos) {
		ExecutorService executor = Executors.newFixedThreadPool(10);
	    Dispatcher dispatcher = new Dispatcher();	    
	    dispatcher.setDataEmpleadosFromJson(fileNameEmployee);
	    dispatcher.setDataCallFromJson(fileNameCall);
	    dispatcher.setReintentos(reintentos);
	    dispatcher.setCallSize(dispatcher.getLlamadas().size());
	    
	    Future<CallResponse> resultado = null;	    
	    dispatcher.setInterruptor(interruptor);
	    for (int i = 0; i < dispatcher.getLlamadas().size(); i++) {
	    	 resultado = executor.submit(dispatcher);
	    	 listResponse.add(resultado);
	    }
	    executor.shutdown();
	}
}
